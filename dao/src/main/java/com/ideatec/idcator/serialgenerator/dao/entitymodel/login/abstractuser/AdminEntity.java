package com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;

@Entity
@DiscriminatorValue(value = "ADMIN")
@Table(name = "AdminDao")
public class AdminEntity extends AbstractUserEntity {
	private static final long serialVersionUID = -5815058566189305208L;

}
