package com.ideatec.idcator.serialgenerator.dao.entitymodel.repository;

import java.util.ArrayList;
import java.util.List;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.AbstractProductEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.LoginEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.RelProductItemEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.UserActionEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct.ItemEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct.ProductEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.UsernamePasswordEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser.AdminEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser.SerialEditorEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser.SerialGeneratorEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser.SerialPrinterEntity;

public class EntityRepository {
	private List<Class<? extends MainEntity>> repository = new ArrayList<>();

	public EntityRepository() {
		repository.add(ItemEntity.class);
		repository.add(ProductEntity.class);
		repository.add(MainEntity.class);
		repository.add(AdminEntity.class);
		repository.add(SerialEditorEntity.class);
		repository.add(SerialGeneratorEntity.class);
		repository.add(SerialPrinterEntity.class);
		repository.add(AbstractUserEntity.class);
		repository.add(UsernamePasswordEntity.class);
		repository.add(AbstractProductEntity.class);
		repository.add(LoginEntity.class);
		repository.add(RelProductItemEntity.class);
		repository.add(UserActionEntity.class);
	}

	public Class<? extends MainEntity> getByName(String name) {
		return repository.stream().filter(cls -> cls.getSimpleName().toLowerCase().startsWith(name.toLowerCase()))
				.findFirst().orElse(null);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> E getObject(String json, String name) {
		Class<? extends MainEntity> clazz = getByName(name);
		return (E) MainEntity.fromString(json, clazz);
	}
	
}
