package com.ideatec.idcator.serialgenerator.dao.basic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.repository.EntityRepository;
import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.repository.ModelRepository;

public class DaoGeneralModel<M extends MainModel> implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger();

	public DaoGeneralModel() {
		LOGGER.info("EntityManager Create Successfully.");
	}

	public <E extends MainEntity> M save(M m) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		E me = new EntityRepository().getObject(m.toString(), m.getClass().getSimpleName());
		dao.save(me);
		m.setId(me.getId());
		return m;
	}

	public <E extends MainEntity> List<M> save(List<M> ms) throws Exception {
		List<M> result = new ArrayList<>();
		ms.forEach(m -> {
			try {
				result.add(save(m));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return result;
	}

	public <E extends MainEntity> List<M> saveInOneTransaction(List<M> ms) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		List<E> tosave = convertToMainEntity(ms);
		return convertToMainModel(dao.saveInOneTransaction(tosave));
	}

	public <E extends MainEntity> M update(M m) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		E me = new EntityRepository().getObject(m.toString(), m.getClass().getSimpleName());
		dao.update(me);
		m.setId(me.getId());
		return m;
	}

	public <E extends MainEntity> List<M> update(List<M> ms) throws Exception {
		List<M> result = new ArrayList<>();
		ms.forEach(m -> {
			try {
				result.add(update(m));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return result;
	}

	public <E extends MainEntity> List<M> updateInOneTransaction(List<M> ms) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		List<E> tosave = convertToMainEntity(ms);
		return convertToMainModel(dao.updateInOneTransaction(tosave));
	}

	public <E extends MainEntity> M delete(M m) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		E me = new EntityRepository().getObject(m.toString(), m.getClass().getSimpleName());
		dao.delete(me);
		return m;
	}

	public <E extends MainEntity> List<M> delete(List<M> ms) throws Exception {
		List<M> result = new ArrayList<>();
		ms.forEach(m -> {
			try {
				result.add(delete(m));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return result;
	}

	public <E extends MainEntity> List<M> deleteInOneTransaction(List<M> ms) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		List<E> tosave = convertToMainEntity(ms);
		return convertToMainModel(dao.deleteInOneTransaction(tosave));
	}

	public <E extends MainEntity> List<M> select(String queryName, Map<String, Object> params) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return convertToMainModel(dao.select(queryName, params));
	}

	public <E extends MainEntity> List<Object> selectMultiType(String queryName, Map<String, Object> params)
			throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return dao.selectMultiType(queryName, params);
	}

	public <E extends MainEntity> Object selectOneObject(String queryName, Map<String, Object> params)
			throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return dao.selectOneObject(queryName, params);
	}

	public <E extends MainEntity> List<M> select(String queryName, Map<String, Object> params, int first, int max)
			throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return convertToMainModel(dao.select(queryName, params, first, max));
	}

	public <E extends MainEntity> M selectSingle(Class<M> m, String queryName, Map<String, Object> params)
			throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		E e = dao.selectSingle(queryName, params);
		return new ModelRepository().getObject(e.toString(), m.getClass().getSimpleName());
	}

	public <E extends MainEntity> Float selectCalculat(String queryName, Map<String, Object> params) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return dao.selectCalculat(queryName, params);
	}

	public <E extends MainEntity> List<M> executeNamedQuery(String queryName, Map<String, Object> params)
			throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return convertToMainModel(dao.executeNamedQuery(queryName, params));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m, int first, int max) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec, first, max));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m, int first, int max, String order) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec, first, max, order));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m, int first, int max, Map<String, Object> filters,
			String sortField, String sortOrder) throws NoSuchFieldException, SecurityException {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec, first, max, filters, sortField, sortOrder));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m, String where, int first, int max) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec, where, first, max));
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> List<M> select(Class<M> m, String afterselect) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return convertToMainModel(dao.select((Class<E>) ec, afterselect));
	}

	public <E extends MainEntity> M select(String className, long id) throws Exception {
		@SuppressWarnings("unchecked")
		Class<M> clazz = (Class<M>) Class.forName(className);
		return select(clazz, id);

	}

	public <E extends MainEntity> M select(Class<M> m, long id) throws Exception {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		E e = dao.select(m.getSimpleName(), id);
		return new ModelRepository().getObject(e.toString(), m.getClass().getSimpleName());
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> float average(Class<M> m, String col, Map<String, String> params) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.average((Class<E>) ec, col, params);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> float sum(Class<M> m, String col, Map<String, String> params) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.sum((Class<E>) ec, col, params);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> float sum(Class<M> m, String col, String where) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.sum((Class<E>) ec, col, where);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> float max(Class<M> m, String col, Map<String, String> params) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.max((Class<E>) ec, col, params);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> float min(Class<M> m, String col, Map<String, String> params) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.min((Class<E>) ec, col, params);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> long count(Class<M> m) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.count((Class<E>) ec);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> long count(Class<M> m, String where) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.count((Class<E>) ec, where);
	}

	public <E extends MainEntity> List<Object> executeQuery(String strQuery) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return dao.executeQuery(strQuery);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> long delete(Class<M> m) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.delete((Class<E>) ec);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> long delete(Class<M> m, long id) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.delete((Class<E>) ec, id);
	}

	@SuppressWarnings("unchecked")
	public <E extends MainEntity> long delete(Class<M> m, String where) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		Class<? extends MainEntity> ec = new EntityRepository().getByName(m.getSimpleName());
		return dao.delete((Class<E>) ec, where);
	}

	public <E extends MainEntity> Object executeQuerySingelResult(String strQuery) {
		DaoGeneralEntity<E> dao = new DaoGeneralEntity<>();
		return dao.executeQuerySingelResult(strQuery);
	}

	private <E extends MainEntity> List<M> convertToMainModel(List<E> mainEntities) {
		List<M> ms = new ArrayList<>();
		mainEntities.forEach(me -> {
			M m = new ModelRepository().getObject(me.toString(), me.getClass().getSimpleName());
			ms.add(m);
		});
		return ms;
	}

	private <E extends MainEntity> List<E> convertToMainEntity(List<M> mainModels) {
		List<E> ms = new ArrayList<>();
		mainModels.forEach(mm -> {
			E me = new EntityRepository().getObject(mm.toString(), mm.getClass().getSimpleName());
			ms.add(me);
		});
		return ms;
	}
}