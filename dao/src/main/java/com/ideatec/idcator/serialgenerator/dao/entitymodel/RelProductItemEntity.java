package com.ideatec.idcator.serialgenerator.dao.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct.ItemEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct.ProductEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;

@Entity
@Table(name = "REL_PRODUCT_ITEM")
public class RelProductItemEntity extends MainEntity {
	private static final long serialVersionUID = 56946904295381180L;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ITEM")
	private ItemEntity itemEntity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUCT")
	private ProductEntity productEntity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USERACTION")
	private UserActionEntity userActionEntity;

	public RelProductItemEntity() {
		super();
	}

	public RelProductItemEntity(ItemEntity itemEntity, ProductEntity productEntity, UserActionEntity userActionEntity) {
		super();
		this.itemEntity = itemEntity;
		this.productEntity = productEntity;
		this.userActionEntity = userActionEntity;
	}

	public RelProductItemEntity(ProductEntity productEntity, ItemEntity itemEntity) {
		this.itemEntity = itemEntity;
		this.productEntity = productEntity;
	}

	public ItemEntity getItem() {
		return itemEntity;
	}

	public void setItem(ItemEntity itemEntity) {
		this.itemEntity = itemEntity;
	}

	public ProductEntity getProduct() {
		return productEntity;
	}

	public void setProduct(ProductEntity productEntity) {
		this.productEntity = productEntity;
	}

	public UserActionEntity getUserAction() {
		return userActionEntity;
	}

	public void setUserAction(UserActionEntity userActionEntity) {
		this.userActionEntity = userActionEntity;
	}

}
