package com.ideatec.idcator.serialgenerator.dao.entitymodel.login;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TYPE", columnDefinition = "TYPE", discriminatorType = DiscriminatorType.STRING)
@Table(name = "ABSTRACT_USER", uniqueConstraints = { @UniqueConstraint(columnNames = { "SERIALNO" }) })
public abstract class AbstractUserEntity extends MainEntity {
	private static final long serialVersionUID = 7244565682397022112L;

	@Column(name = "FIRSTNAME")
	private String firstname;

	@Column(name = "LASTNAME")
	private String lastname;

	@Column(name = "SERIALNO")
	private String serialno;

	public AbstractUserEntity() {
		super();
	}

	public AbstractUserEntity(String firstname, String lastname, String serialno) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.serialno = serialno;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

}
