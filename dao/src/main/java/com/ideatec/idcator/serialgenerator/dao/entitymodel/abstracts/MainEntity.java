package com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.google.gson.Gson;

@MappedSuperclass
public abstract class MainEntity implements Cloneable, Serializable {
	private static final long serialVersionUID = -1486519919077639334L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected long id;

	@Version
	@Column(name = "VERSION")
	protected long version;

	@Column(name = "CREATE_ON")
	protected long createOn;

	@Column(name = "UPDATE_ON")
	protected long updateOn;

	@Column(name = "CREATE_USER_ID")
	protected long createUserId;

	@Column(name = "UPDATE_USER_ID")
	protected long updateUserId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getCreateOn() {
		return createOn;
	}

	public void setCreateOn(long createOn) {
		this.createOn = createOn;
	}

	public long getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(long updateOn) {
		this.updateOn = updateOn;
	}

	public long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(long createUserId) {
		this.createUserId = createUserId;
	}

	public long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(long updateUserId) {
		this.updateUserId = updateUserId;
	}

	@PrePersist
	public void createDate() {
		updateOn = createOn = Calendar.getInstance().getTimeInMillis();
	}

	@PreUpdate
	public void updateDate() {
		updateOn = Calendar.getInstance().getTimeInMillis();
	}

	@Override
	@Transient
	public String toString() {
		return new Gson().toJson(this);
	}

	@Transient
	public static <E extends MainEntity> E fromString(String json, Class<E> clazz) {
		return new Gson().fromJson(json, clazz);
	}

	@Override
	@Transient
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
