package com.ideatec.idcator.serialgenerator.dao.basic;

import java.util.List;
import java.util.Map;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;

public abstract class Dao<E extends MainEntity> {
	protected DaoGeneralEntity<E> dao;

	public Dao() {
		dao = new DaoGeneralEntity<>();
	}

	public E save(E e) throws Exception {
		return dao.save(e);
	}

	public List<E> save(List<E> es) throws Exception {
		return dao.save(es);
	}

	public List<E> saveInOneTransaction(List<E> es) throws Exception {
		return dao.saveInOneTransaction(es);
	}

	public E update(E e) throws Exception {
		return dao.update(e);
	}

	public List<E> update(List<E> es) throws Exception {
		return dao.update(es);
	}

	public List<E> updateInOneTransaction(List<E> ts) throws Exception {
		return dao.updateInOneTransaction(ts);
	}

	public E delete(E e) throws Exception {
		return dao.delete(e);
	}

	public List<E> delete(List<E> ts) throws Exception {
		return dao.delete(ts);
	}

	public List<E> deleteInOneTransaction(List<E> ts) throws Exception {
		return dao.deleteInOneTransaction(ts);
	}

	public List<E> select(String queryName, Map<String, Object> params) throws Exception {
		return dao.select(queryName, params);
	}

	public List<Object> selectMultiType(String queryName, Map<String, Object> params) throws Exception {
		return dao.selectMultiType(queryName, params);
	}

	public Object selectOneObject(String queryName, Map<String, Object> params) throws Exception {
		return dao.selectOneObject(queryName, params);
	}

	public List<E> select(String queryName, Map<String, Object> params, int first, int max) throws Exception {
		return dao.select(queryName, params, first, max);
	}

	public E selectSingle(String queryName, Map<String, Object> params) throws Exception {
		return dao.selectSingle(queryName, params);
	}

	public Float selectCalculat(String queryName, Map<String, Object> params) throws Exception {
		return dao.selectCalculat(queryName, params);
	}

	public List<E> executeNamedQuery(String queryName, Map<String, Object> params) throws Exception {
		return dao.executeNamedQuery(queryName, params);
	}

	public List<E> select(Class<E> e, int first, int max) {
		return dao.select(e, first, max);
	}

	public List<E> select(Class<E> e, int first, int max, String order) {
		return dao.select(e, first, max, order);
	}

	public List<E> select(Class<E> e, int first, int max, Map<String, Object> filters, String sortField,
			String sortOrder) throws NoSuchFieldException, SecurityException {
		return dao.select(e, first, max, filters, sortField, sortOrder);
	}

	public List<E> select(Class<E> e, String where, int first, int max) {
		return dao.select(e, where, first, max);
	}

	public List<E> select(Class<E> e, String afterselect) {
		return dao.select(e, afterselect);
	}

	public E select(String className, long id) throws Exception {
		return dao.select(className, id);
	}

	public E select(Class<E> e, long id) {
		return dao.select(e, id);
	}

	public float average(Class<E> e, String col, Map<String, String> params) {
		return dao.average(e, col, params);
	}

	public float sum(Class<E> e, String col, Map<String, String> params) {
		return dao.sum(e, col, params);
	}

	public float sum(Class<E> e, String col, String where) {
		return dao.sum(e, col, where);
	}

	public float max(Class<E> e, String col, Map<String, String> params) {
		return dao.max(e, col, params);
	}

	public float min(Class<E> e, String col, Map<String, String> params) {
		return dao.min(e, col, params);
	}

	public long count(Class<E> e) {
		return dao.count(e);
	}

	public long count(Class<E> e, String where) {
		return dao.count(e, where);
	}

	public List<Object> executeQuery(String strQuery) {
		return dao.executeQuery(strQuery);
	}

	public long delete(Class<E> e) {
		return dao.delete(e);
	}

	public long delete(Class<E> e, long id) {
		return dao.delete(e, id);
	}

	public long delete(Class<E> e, String where) {
		return dao.delete(e, where);
	}

	public Object executeQuerySingelResult(String strQuery) {
		return dao.executeQuerySingelResult(strQuery);
	}

}
