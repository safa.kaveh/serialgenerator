package com.ideatec.idcator.serialgenerator.dao.entitymodel;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TYPE", columnDefinition = "TYPE", discriminatorType = DiscriminatorType.STRING)
@Table(name = "ABSTRACT_PRODUCT", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME", "CODE" }) })
public abstract class AbstractProductEntity extends MainEntity {
	private static final long serialVersionUID = 111103707370393011L;

	@Column(name = "NAME")
	private String name;

	@Column(name = "CODE")
	private int code;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "DETAILS")
	private String details;

	public AbstractProductEntity() {
		super();
	}

	public AbstractProductEntity(String name, int code, String type, String details) {
		super();
		this.name = name;
		this.code = code;
		this.type = type;
		this.details = details;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
