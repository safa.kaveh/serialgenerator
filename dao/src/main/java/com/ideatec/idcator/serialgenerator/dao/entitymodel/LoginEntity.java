package com.ideatec.idcator.serialgenerator.dao.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.UsernamePasswordEntity;

@Entity
@Table(name = "LoginDao")
public class LoginEntity extends MainEntity {
	private static final long serialVersionUID = 1L;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ABSTRACT_USER")
	private AbstractUserEntity abstractUserEntity;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USERNAME_PASSWORD")
	private UsernamePasswordEntity usernamePasswordEntity;

	public LoginEntity() {
		super();
	}

	public LoginEntity(AbstractUserEntity abstractUserEntity, UsernamePasswordEntity usernamePasswordEntity) {
		super();
		this.abstractUserEntity = abstractUserEntity;
		this.usernamePasswordEntity = usernamePasswordEntity;
	}

	public AbstractUserEntity getAbstractUser() {
		return abstractUserEntity;
	}

	public void setAbstractUser(AbstractUserEntity abstractUserEntity) {
		this.abstractUserEntity = abstractUserEntity;
	}

	public UsernamePasswordEntity getUsernamePassword() {
		return usernamePasswordEntity;
	}

	public void setUsernamePassword(UsernamePasswordEntity usernamePasswordEntity) {
		this.usernamePasswordEntity = usernamePasswordEntity;
	}

}
