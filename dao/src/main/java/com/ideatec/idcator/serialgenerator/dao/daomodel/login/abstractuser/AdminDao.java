package com.ideatec.idcator.serialgenerator.dao.daomodel.login.abstractuser;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.Admin;

public class AdminDao extends DaoGeneralModel<Admin> {
	private static final long serialVersionUID = 4224964871583825003L;

}
