package com.ideatec.idcator.serialgenerator.dao.daomodel.login;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;

public abstract class AbstractUserDao extends DaoGeneralModel<AbstractUser> {
	private static final long serialVersionUID = 7528217911965164017L;

}
