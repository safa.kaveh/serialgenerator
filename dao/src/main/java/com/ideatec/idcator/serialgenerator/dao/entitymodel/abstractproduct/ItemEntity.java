package com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.AbstractProductEntity;

@Entity
@DiscriminatorValue(value = "ITEM")
@Table(name = "ITEM")
public class ItemEntity extends AbstractProductEntity {
	private static final long serialVersionUID = 1L;

}
