package com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;


@Entity
@DiscriminatorValue(value = "SERIAL_EDITOR")
@Table(name = "SERIAL_EDITOR")
public class SerialEditorEntity extends AbstractUserEntity {
	private static final long serialVersionUID = 4490171004429674577L;

}
