package com.ideatec.idcator.serialgenerator.dao.entitymodel.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;

@Entity
@Table(name = "USERNAMES_PASSWORDS", uniqueConstraints = { @UniqueConstraint(columnNames = { "USERNAME" }),
		@UniqueConstraint(columnNames = { "TOKEN" }) })
public class UsernamePasswordEntity extends MainEntity {
	private static final long serialVersionUID = -457861800744511287L;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "TOKEN")
	private String token;

	public UsernamePasswordEntity() {
		super();
	}

	public UsernamePasswordEntity(String username, String password, String token) {
		super();
		this.username = username;
		this.password = password;
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
