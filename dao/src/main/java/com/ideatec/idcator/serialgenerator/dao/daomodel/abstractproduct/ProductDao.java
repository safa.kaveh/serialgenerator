package com.ideatec.idcator.serialgenerator.dao.daomodel.abstractproduct;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Product;

public class ProductDao extends DaoGeneralModel<Product> {
	private static final long serialVersionUID = -609903438421170045L;

}
