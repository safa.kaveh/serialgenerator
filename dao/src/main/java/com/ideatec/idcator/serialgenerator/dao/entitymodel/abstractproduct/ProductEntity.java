package com.ideatec.idcator.serialgenerator.dao.entitymodel.abstractproduct;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.AbstractProductEntity;

@Entity
@DiscriminatorValue(value = "PRODUCT")
@Table(name = "PRODUCT")
public class ProductEntity extends AbstractProductEntity {
	private static final long serialVersionUID = 1L;

}
