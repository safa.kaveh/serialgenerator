package com.ideatec.idcator.serialgenerator.dao.daomodel.abstractproduct;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Item;

public class ItemDao extends DaoGeneralModel<Item> {
	private static final long serialVersionUID = 1770074543034343934L;

}
