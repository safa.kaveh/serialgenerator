package com.ideatec.idcator.serialgenerator.dao.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.abstracts.MainEntity;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.enums.EnumAction;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;

@Entity
@Table(name = "USER_ACTION")
public class UserActionEntity extends MainEntity {
	private static final long serialVersionUID = 3483671687166013218L;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ABSTRACTUSER")
	private AbstractUserEntity user;

	@Enumerated(EnumType.STRING)
	@Column(name = "ENUM_ACTION")
	private EnumAction enumAction;

	public UserActionEntity() {
		super();
	}

	public UserActionEntity(AbstractUserEntity user, EnumAction enumAction) {
		super();
		this.user = user;
		this.enumAction = enumAction;
	}

	public AbstractUserEntity getUser() {
		return user;
	}

	public void setUser(AbstractUserEntity user) {
		this.user = user;
	}

	public EnumAction getEnumAction() {
		return enumAction;
	}

	public void setEnumAction(EnumAction enumAction) {
		this.enumAction = enumAction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
