package com.ideatec.idcator.serialgenerator.dao.daomodel.login.abstractuser;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.SerialGenerator;

public class SerialGeneratorDao extends DaoGeneralModel<SerialGenerator> {
	private static final long serialVersionUID = 5842691894603970305L;

}
