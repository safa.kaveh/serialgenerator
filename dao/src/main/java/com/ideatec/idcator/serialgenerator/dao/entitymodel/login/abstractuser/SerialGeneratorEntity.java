package com.ideatec.idcator.serialgenerator.dao.entitymodel.login.abstractuser;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.AbstractUserEntity;

@Entity
@DiscriminatorValue(value = "SERIAL_GENERATOR")
@Table(name = "SERIAL_GENERATOR")
public class SerialGeneratorEntity extends AbstractUserEntity {
	private static final long serialVersionUID = -4884576582277972016L;

}
