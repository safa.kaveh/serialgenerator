package com.ideatec.idcator.serialgenerator.dao.daomodel;

import com.ideatec.idcator.serialgenerator.dao.basic.DaoGeneralModel;
import com.ideatec.idcator.serialgenerator.model.AbstractProduct;

public abstract class AbstractProductDao extends DaoGeneralModel<AbstractProduct> {
	private static final long serialVersionUID = 1114341118408730952L;

}
