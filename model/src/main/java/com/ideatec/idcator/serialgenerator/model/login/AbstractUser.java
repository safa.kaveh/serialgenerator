package com.ideatec.idcator.serialgenerator.model.login;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public abstract class AbstractUser extends MainModel {
	private static final long serialVersionUID = 7244565682397022112L;

	private String firstname;

	private String lastname;

	private String serialno;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

}
