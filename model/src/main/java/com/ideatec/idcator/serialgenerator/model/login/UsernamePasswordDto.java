package com.ideatec.idcator.serialgenerator.model.login;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public class UsernamePasswordDto extends MainModel implements UsernamePassword {
	private static final long serialVersionUID = -457861800744511287L;

	private String username;

	private String password;

	private String token;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
