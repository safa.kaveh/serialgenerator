package com.ideatec.idcator.serialgenerator.model.abstracts;

import java.io.Serializable;

import com.google.gson.Gson;

public abstract class MainModel implements Serializable {
	private static final long serialVersionUID = 5920491796222453331L;

	protected long id;

	protected long version;

	protected long createOn;

	protected long updateOn;

	protected long createUserId;

	protected long updateUserId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getCreateOn() {
		return createOn;
	}

	public void setCreateOn(long createOn) {
		this.createOn = createOn;
	}

	public long getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(long updateOn) {
		this.updateOn = updateOn;
	}

	public long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(long createUserId) {
		this.createUserId = createUserId;
	}

	public long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(long updateUserId) {
		this.updateUserId = updateUserId;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	public static <M extends MainModel> M fromString(String json, Class<M> clazz) {
		return new Gson().fromJson(json, clazz);
	}

}
