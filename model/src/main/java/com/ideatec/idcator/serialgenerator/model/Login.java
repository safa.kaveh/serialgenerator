package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;
import com.ideatec.idcator.serialgenerator.model.login.UsernamePasswordDto;

public class Login extends MainModel {
	private static final long serialVersionUID = 3225365086896333041L;

	private AbstractUser abstractUser;

	private UsernamePasswordDto usernamePasswordDto;

	public Login() {
		super();
	}

	public Login(AbstractUser abstractUser, UsernamePasswordDto usernamePasswordDto) {
		super();
		this.abstractUser = abstractUser;
		this.usernamePasswordDto = usernamePasswordDto;
	}

	public AbstractUser getAbstractUser() {
		return abstractUser;
	}

	public void setAbstractUser(AbstractUser abstractUser) {
		this.abstractUser = abstractUser;
	}

	public UsernamePasswordDto getUsernamePassword() {
		return usernamePasswordDto;
	}

	public void setUsernamePassword(UsernamePasswordDto usernamePasswordDto) {
		this.usernamePasswordDto = usernamePasswordDto;
	}

}
