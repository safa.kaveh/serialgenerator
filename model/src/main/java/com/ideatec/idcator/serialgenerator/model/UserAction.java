package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.enums.EnumAction;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;

public class UserAction extends MainModel {
	private static final long serialVersionUID = 6405672530265080445L;

	private AbstractUser user;

	private EnumAction enumAction;

	public AbstractUser getUser() {
		return user;
	}

	public void setUser(AbstractUser user) {
		this.user = user;
	}

	public EnumAction getEnumAction() {
		return enumAction;
	}

	public void setEnumAction(EnumAction enumAction) {
		this.enumAction = enumAction;
	}

}
