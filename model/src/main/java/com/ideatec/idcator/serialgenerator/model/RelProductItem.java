package com.ideatec.idcator.serialgenerator.model;

import com.google.gson.Gson;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Item;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Product;
import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public class RelProductItem extends MainModel {
	private static final long serialVersionUID = -6568651251130368749L;

	private Item item;

	private Product product;

	private UserAction userAction;

	public RelProductItem() {
		super();
	}

	public RelProductItem(String json) {
		this(new Gson().fromJson(json, RelProductItem.class));
	}

	public RelProductItem(RelProductItem relProductItem) {
		this(relProductItem.product, relProductItem.item);
		this.setId(relProductItem.getId());
	}

	public RelProductItem(Product product, Item item) {
		this.item = item;
		this.product = product;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public UserAction getUserAction() {
		return userAction;
	}

	public void setUserAction(UserAction userAction) {
		this.userAction = userAction;
	}

}
