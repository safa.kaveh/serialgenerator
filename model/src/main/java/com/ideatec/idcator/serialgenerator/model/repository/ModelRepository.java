package com.ideatec.idcator.serialgenerator.model.repository;

import java.util.ArrayList;
import java.util.List;

import com.ideatec.idcator.serialgenerator.model.AbstractProduct;
import com.ideatec.idcator.serialgenerator.model.Login;
import com.ideatec.idcator.serialgenerator.model.RelProductItem;
import com.ideatec.idcator.serialgenerator.model.UserAction;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Item;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Product;
import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;
import com.ideatec.idcator.serialgenerator.model.login.UsernamePasswordDto;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.Admin;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.SerialEditor;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.SerialGenerator;
import com.ideatec.idcator.serialgenerator.model.login.abstractuser.SerialPrinter;

public class ModelRepository {
	private List<Class<? extends MainModel>> repository = new ArrayList<>();

	public ModelRepository() {
		repository.add(Item.class);
		repository.add(Product.class);
		repository.add(MainModel.class);
		repository.add(Admin.class);
		repository.add(SerialEditor.class);
		repository.add(SerialGenerator.class);
		repository.add(SerialPrinter.class);
		repository.add(AbstractUser.class);
		repository.add(UsernamePasswordDto.class);
		repository.add(AbstractProduct.class);
		repository.add(Login.class);
		repository.add(RelProductItem.class);
		repository.add(UserAction.class);
	}

	public Class<? extends MainModel> getByName(String name) {
		return repository.stream()
				.filter(cls -> cls.getSimpleName().toLowerCase()
						.startsWith(name.toLowerCase().replace("entity", "").replace("dao", "")))
				.findFirst().orElse(null);
	}

	@SuppressWarnings("unchecked")
	public <M extends MainModel> M getObject(String json, String name) {
		Class<? extends MainModel> clazz = getByName(name);
		return (M) MainModel.fromString(json, clazz);
	}
}
