package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public abstract class AbstractProduct extends MainModel {
	private static final long serialVersionUID = 7088767686090302764L;

	private String name;

	private int code;

	private String type;

	private String details;

	public AbstractProduct() {
		super();
	}

	public AbstractProduct(String name, int code, String type, String details) {
		super();
		this.name = name;
		this.code = code;
		this.type = type;
		this.details = details;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
