package com.ideatec.idcator.serialgenerator.webservice.business.external.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class Test {

	@GET
	@Path("/")
	public Response test() {
		// org.postgresql.ds.PGSimpleDataSource
		
		return Response.ok("Service start successfully.").build();
	}
}
