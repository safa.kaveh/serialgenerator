package com.ideatec.idcator.serialgenerator.webservice.business.external.rest.authentication;

import java.security.Key;
import java.util.Base64;
import java.util.Date;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.ideatec.idcator.serialgenerator.business.service.internal.model.LoginService;
import com.ideatec.idcator.serialgenerator.model.Login;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Path(value = "/authentication")
public class Authentication {
	private static final String AUTHENTICATION_SCHEME = "Basic";
	private static final int ttl = 30 * 60 * 1000;

	@POST
	@Path("/")
	public Response getToken(ContainerRequestContext requestContext) {
		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).trim();

		if (!isTokenBasedAuthentication(authorizationHeader)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();
		return validateToken(token);
	}

	@POST
	@Path("/form-data")
	public Response getToken(@FormParam("username") String username, @FormParam("password") String password) {
		String token = (Base64.getEncoder().encodeToString((username + ":" + password).getBytes()));
		return validateToken(token);
	}

	private boolean isTokenBasedAuthentication(String authorizationHeader) {
		boolean isTokenBasedAuthentication = authorizationHeader != null
				&& authorizationHeader.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
		return isTokenBasedAuthentication;
	}

	private Response validateToken(String token) {
		Login login = isValidateToken(token);
		if (login == null || login.getId() == 0) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		} else {
			return Response.ok(createJWT(String.valueOf(login.getId()), login.getUsernamePassword().getUsername(),
					"authentication", ttl)).build();
		}
	}

	private Login isValidateToken(String token) {
		String[] usernamepassword = new String(Base64.getDecoder().decode(token)).split(":");
		String un = usernamepassword[0];
		String pas = usernamepassword[1];
		// TODO Connect to database and checked user exist or no
		Login login = null;
		try {
			//login = new LoginService().select(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return login;
	}

	private String createJWT(String id, String issuer, String subject, long ttlMillis) {

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(now).setSubject(subject).setIssuer(issuer)
				.signWith(key);

		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		return builder.compact();
	}
}
