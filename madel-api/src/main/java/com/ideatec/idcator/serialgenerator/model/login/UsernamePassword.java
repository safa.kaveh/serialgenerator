package com.ideatec.idcator.serialgenerator.model.login;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public interface UsernamePassword  {

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public String getToken();

	public void setToken(String token);

}
