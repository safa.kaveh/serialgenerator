package com.ideatec.idcator.serialgenerator.model.abstracts;

import java.io.Serializable;

public abstract interface MainModel extends Serializable {
	public long getId();

	public void setId(long id);

	public long getVersion();

	public void setVersion(long version);

	public long getCreateOn();

	public void setCreateOn(long createOn);

	public long getUpdateOn();

	public void setUpdateOn(long updateOn);

	public long getCreateUserId();

	public void setCreateUserId(long createUserId);

	public long getUpdateUserId();

	public void setUpdateUserId(long updateUserId);

	public String toString();

}
