package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public abstract interface AbstractProduct extends MainModel {

	public String getName();

	public void setName(String name);

	public String getDetails();

	public void setDetails(String details);

	public String getType();

	public void setType(String type);

	public int getCode();

	public void setCode(int code);

}
