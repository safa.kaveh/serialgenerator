package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstractproduct.Item;
import com.ideatec.idcator.serialgenerator.model.abstractproduct.Product;
import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public interface RelProductItem extends MainModel {
	public Item getItem();

	public void setItem(Item item);

	public Product getProduct();

	public void setProduct(Product product);

	public UserAction getUserAction();

	public void setUserAction(UserAction userAction);

}
