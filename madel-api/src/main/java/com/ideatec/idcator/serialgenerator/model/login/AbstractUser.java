package com.ideatec.idcator.serialgenerator.model.login;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public abstract interface AbstractUser extends MainModel {

	public String getFirstname();

	public void setFirstname(String firstname);

	public String getLastname();

	public void setLastname(String lastname);

	public String getSerialno();

	public void setSerialno(String serialno);

}
