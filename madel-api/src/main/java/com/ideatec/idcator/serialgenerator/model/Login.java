package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;
import com.ideatec.idcator.serialgenerator.model.login.UsernamePassword;

public interface Login extends MainModel {
	public AbstractUser getAbstractUser();

	public void setAbstractUser(AbstractUser abstractUser);

	public UsernamePassword getUsernamePassword();

	public void setUsernamePassword(UsernamePassword usernamePassword);
}
