package com.ideatec.idcator.serialgenerator.model;

import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;
import com.ideatec.idcator.serialgenerator.model.enums.EnumAction;
import com.ideatec.idcator.serialgenerator.model.login.AbstractUser;

public interface UserAction extends MainModel {

	public AbstractUser getUser();

	public void setUser(AbstractUser user);

	public EnumAction getEnumAction();

	public void setEnumAction(EnumAction enumAction);

}
