package com.ideatec.idcator.serialgenerator.business.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class DataGenerator {
	private String message = "";
	private final static String FILE_NAME = "messages.properties";

	public DataGenerator() throws IOException {
		File file = new File(FILE_NAME);
		Properties properties = new Properties();
		if (file.exists()) {
			FileInputStream fis = new FileInputStream(FILE_NAME);
			properties.load(fis);
			message = properties.getProperty("message");
			fis.close();
		}
	}

	public String generate() {
		JalaliCalendar jc = new JalaliCalendar();
		return null;
	}

	public void setMessage(String message) throws IOException {
		Properties prop = new Properties();
		FileOutputStream fos = new FileOutputStream(FILE_NAME);
		prop.setProperty("message", message);
		prop.store(fos, null);
		fos.flush();
		fos.close();
		this.message = message;
	}
}
