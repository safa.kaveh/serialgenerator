package com.ideatec.idcator.serialgenerator.business.service.internal.model.login;

import com.ideatec.idcator.serialgenerator.dao.daomodel.login.AbstractUserDao;

public abstract class AbstractUserService extends AbstractUserDao {
	private static final long serialVersionUID = -89071486456384L;
}