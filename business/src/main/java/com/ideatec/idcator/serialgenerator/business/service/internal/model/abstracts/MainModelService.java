package com.ideatec.idcator.serialgenerator.business.service.internal.model.abstracts;

import com.ideatec.idcator.serialgenerator.dao.daomodel.abstracts.MainModelDao;
import com.ideatec.idcator.serialgenerator.model.abstracts.MainModel;

public abstract class MainModelService extends MainModelDao<MainModel> {

}
