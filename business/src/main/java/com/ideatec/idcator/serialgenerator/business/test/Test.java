package com.ideatec.idcator.serialgenerator.business.test;

import java.awt.image.BufferedImage;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.ideatec.idcator.serialgenerator.business.logic.JalaliCalendar;

public class Test {

	public static void main(String[] args) {
		JalaliCalendar jc = new JalaliCalendar();
		System.out.println(jc.getJalali("MMddyyyy").replace("'", ""));
/*
		int base = 100;
		long l = 12319999;
		int c = 0;
		String result = "";
		while (l != 0) {
			result += String.format("%02d", l % base);

			l /= base;
		}

		l = Long.MAX_VALUE;
		while (l != 0) {
			result += String.format("%02d", l % base);
			l /= base;
		}
		result = "Ideatec Parmis\nWeb: http://idea-tec.com/\n"
				+ Base64.getEncoder().withoutPadding().encodeToString(result.getBytes());
		System.out.println(result);
		try {
			BufferedImage bi = BarcodeGenerator.generateQRCodeImagePng(result, 171);
			BarcodeGenerator.save(bi, "./mybarcode.png");
		} catch (WriterException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}

	public static BufferedImage encode(String contents, int width, int height) {
		int codeWidth = 3 + // start guard
				(7 * 6) + // left bars
				5 + // middle guard
				(7 * 6) + // right bars
				3; // end guard
		codeWidth = Math.max(codeWidth, width);
		try {
			// 原为13位Long型数字（BarcodeFormat.EAN_13），现为128位
			BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.CODE_128, codeWidth, height,
					null);
			return MatrixToImageWriter.toBufferedImage(bitMatrix);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
