package com.ideatec.idcator.serialgenerator.business.logic;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class BarcodeGenerator {

	public static BufferedImage generateQRCodeImagePng(String text, int width, int height)
			throws WriterException, IOException {
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
		return MatrixToImageWriter.toBufferedImage(bitMatrix);
	}

	public static BufferedImage generateQRCodeImagePng(String text, int size) throws WriterException, IOException {
		return generateQRCodeImagePng(text, size, size);
	}

	public static byte[] generateQRCodePngByteArray(String text, int size) throws WriterException, IOException {
		BufferedImage bi = generateQRCodeImagePng(text, size);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "png", baos);
		baos.flush();
		return baos.toByteArray();
	}

	public static void save(BufferedImage bufferedImage, String filePath) throws WriterException, IOException {
		File outputfile = new File(filePath);
		ImageIO.write(bufferedImage, "png", outputfile);

	}
}
