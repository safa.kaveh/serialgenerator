package com.ideatec.idcator.serialgenerator.business.service.internal.model.login;

import org.modelmapper.ModelMapper;

import com.ideatec.idcator.serialgenerator.dao.daomodel.login.UsernamePasswordDao;
import com.ideatec.idcator.serialgenerator.dao.entitymodel.login.UsernamePasswordEntity;
import com.ideatec.idcator.serialgenerator.model.login.UsernamePasswordDto;

public class UsernamePasswordService extends UsernamePasswordDao {
	private static final long serialVersionUID = 5872598234235534592L;

	public UsernamePasswordDto save(UsernamePasswordDto e) throws Exception {
		ModelMapper modelMapper = new ModelMapper();
		UsernamePasswordEntity entity = modelMapper.map(e, UsernamePasswordEntity.class);
		return modelMapper.map(super.save(entity), UsernamePasswordDto.class);
	}
}
