package com.ideatec.idcator.serialgenerator.business.service.internal.model;

import com.ideatec.idcator.serialgenerator.dao.daomodel.AbstractProductDao;

public abstract class AbstractProductService extends AbstractProductDao {
	private static final long serialVersionUID = 1L;
}