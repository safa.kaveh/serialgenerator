package com.ideatec.idcator.serialgenerator.business.logic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class JalaliCalendar extends GregorianCalendar {

	private static final long serialVersionUID = 6442650686217750455L;

	private final static int BASE_NUMBER = 0x3765a;
	public final static int JALALI = 1;
	public final static int GREGORIAN = 2;
	public final static int JALALI_YAER = 100;
	public final static int JALALI_MONTH = 101;
	public final static int JALALI_DAY = 102;
	public final static int JALALI_WEEK_OF_MONTH = 103;
	public final static int JALALI_WEEK_OF_YEAR = 104;
	public final static int JALALI_DAY_OF_WEEK = 105;
	public final static int JALALI_DAY_OF_WEEK_IN_MONTH = 106;
	public final static int JALALI_DAY_OF_MONTH = 107;
	public final static int JALALI_DAY_OF_YEAR = 108;
	public final static int JALALI_FIRST_DAY_OF_YEAR = 109;
	public final static int JALALI_FIRST_DAY_OF_MONTH = 110;

	public JalaliCalendar() {
		Calendar c = Calendar.getInstance();
		super.setTimeInMillis(c.getTimeInMillis());

	}

	public JalaliCalendar(Calendar calendar) {
		super.setTimeInMillis(calendar.getTimeInMillis());

	}

	public JalaliCalendar(long millisecond) {
		super.setTimeInMillis(millisecond);
	}

	public JalaliCalendar(GregorianCalendar calendar) {
		super.setTimeInMillis(calendar.getTimeInMillis());
	}

	public JalaliCalendar(TimeZone zone) {
		super(zone);
	}

	public JalaliCalendar(Locale aLocale) {
		super(aLocale);
	}

	public JalaliCalendar(TimeZone zone, Locale aLocale) {
		super(zone, aLocale);
	}

	public JalaliCalendar(int year, int month, int dayOfMonth) {
		super(year, month, dayOfMonth);
	}

	public JalaliCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute) {
		super(year, month, dayOfMonth, hourOfDay, minute);
	}

	public JalaliCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute, int second) {
		super(year, month, dayOfMonth, hourOfDay, minute, second);
	}

	public JalaliCalendar(String strJalali) {
		setJalaliCalendar(strJalali, '/');
	}

	// ======================= Start Setters =======================
	public JalaliCalendar setJalaliCalendar(Calendar calendar) {
		this.setTimeInMillis(calendar.getTimeInMillis());
		return this;
	}

	public JalaliCalendar setJalaliCalendar(GregorianCalendar calendar) {
		this.setTimeInMillis(calendar.getTimeInMillis());
		return this;
	}

	public JalaliCalendar setJalaliCalendar(int year, int month, int dayOfMonth) {
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month);
		this.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		return this;
	}

	public JalaliCalendar setJalaliCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute) {
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month);
		this.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		this.set(Calendar.HOUR_OF_DAY, hourOfDay);
		this.set(Calendar.MINUTE, minute);
		return this;
	}

	public JalaliCalendar setJalaliCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute,
			int second) {
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month);
		this.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		this.set(Calendar.HOUR_OF_DAY, hourOfDay);
		this.set(Calendar.MINUTE, minute);
		this.set(Calendar.SECOND, second);
		return this;
	}

	public JalaliCalendar setJalaliCalendar(String strJalali) {
		return setJalaliCalendar(strJalali, '/');
	}

	public JalaliCalendar setJalaliCalendar(String strJalali, char separator) {
		String[] strs = strJalali.split(String.valueOf(separator));
		setGregorianChange(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]), Integer.parseInt(strs[2]));
		return this;
	}

	public JalaliCalendar setJalaliCalendar(String str, int type) {
		return setJalaliCalendar(str, '/', type);
	}

	public JalaliCalendar setJalaliCalendar(String str, char separator, int type) {
		String[] strs = str.split(String.valueOf(separator));
		if (type == 1) {
			setJalaliCalendar(str, separator);
		} else if (type == 2) {
			super.set(Calendar.YEAR, Integer.parseInt(strs[0]));
			super.set(Calendar.MONTH, Integer.parseInt(strs[1]));
			super.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strs[2]));
		}
		return this;
	}

	// ======================= End Setters =========================

	// ======================= Start Getters =======================
	public String getJalali() {
		return toString();
	}

	public String getJalali(String format) {
		return toString(format);
	}

	public String getGregorian() {
		return getGregorian("yyyy/MM/dd");
	}

	public Calendar getGregorianCalendar() {
		setGregorianYearMonthDayWith(getGregorianCurrentCountDay());
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, this.get(Calendar.YEAR));
		c.set(Calendar.MONTH, this.get(Calendar.MONTH));
		c.set(Calendar.DAY_OF_MONTH, this.get(Calendar.DAY_OF_MONTH));
		return c;
	}

	public String getGregorian(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(super.getTime());
	}

	// ======================= End Getters =======================

	// ======================= Start Add and Sub =======================
	public JalaliCalendar JalaliAfter(int jyear, int jmonth, int jdayOfMonth) {
		JalaliAfter(jyear, jmonth, jdayOfMonth, 0, 0, 0);
		return this;
	}

	public JalaliCalendar JalaliAfter(int jyear, int jmonth, int jdayOfMonth, int jhourOfDay, int jminute) {
		JalaliAfter(jyear, jmonth, jdayOfMonth, jhourOfDay, jminute, 0);
		return this;
	}

	public JalaliCalendar JalaliAfter(int jyear, int jmonth, int jdayOfMonth, int jhourOfDay, int jminute,
			int jsecond) {
		final int ONE_SECOND = 1000;
		final int ONE_MINUTE = 60 * ONE_SECOND;
		final int ONE_HOUR = 60 * ONE_MINUTE;
		final long ONE_DAY = 24 * ONE_HOUR;
		long ms = (getJalaliCountDays(jyear, jmonth, jdayOfMonth) * ONE_DAY) + (jminute * ONE_MINUTE)
				+ (jsecond * ONE_SECOND);
		this.setTimeInMillis(this.getTimeInMillis() + ms);
		return this;
	}

	public JalaliCalendar JalaliBefore(int jyear, int jmonth, int jdayOfMonth) {
		JalaliBefore(jyear, jmonth, jdayOfMonth, 0, 0, 0);
		return this;
	}

	public JalaliCalendar JalaliBefore(int jyear, int jmonth, int jdayOfMonth, int jhourOfDay, int jminute) {
		JalaliBefore(jyear, jmonth, jdayOfMonth, jhourOfDay, jminute, 0);
		return this;
	}

	public JalaliCalendar JalaliBefore(int jyear, int jmonth, int jdayOfMonth, int jhourOfDay, int jminute,
			int jsecond) {
		final int ONE_SECOND = 1000;
		final int ONE_MINUTE = 60 * ONE_SECOND;
		final int ONE_HOUR = 60 * ONE_MINUTE;
		final long ONE_DAY = 24 * ONE_HOUR;
		long ms = (getJalaliCountDays(jyear, jmonth, jdayOfMonth) * ONE_DAY) + (jminute * ONE_MINUTE)
				+ (jsecond * ONE_SECOND);
		this.setTimeInMillis(this.getTimeInMillis() - ms);
		return this;
	}

	// ======================= End Add and Sub =========================

	// ======================= Start Jalali Part =======================
	public boolean isJalaliLeapYear(int year) {
		return ((year % 33 == 1) || (year % 33 == 5) || (year % 33 == 9) || (year % 33 == 13) || (year % 33 == 17)
				|| (year % 33 == 22) || (year % 33 == 26) || (year % 33 == 30));
	}

	public boolean isJalaliLeapYear() {
		int year = get(JalaliCalendar.JALALI_YAER);
		return ((year % 33 == 1) || (year % 33 == 5) || (year % 33 == 9) || (year % 33 == 13) || (year % 33 == 17)
				|| (year % 33 == 22) || (year % 33 == 26) || (year % 33 == 30));
	}

	private int getJalaliCountLeap(int year) {
		int result = 0;
		for (int i = 1; i < year; i++)
			if (isJalaliLeapYear(i))
				result++;
		return result;
	}

	private int[] getDayOfMonth(long days) {
		int[] result = new int[2];
		int y = getDaysOfYear(days)[0];
		long last = getDaysOfYear(days)[1];
		int m = 1;
		while (last > getJalaliMonthLength(y, m)) {
			last -= getJalaliMonthLength(y, m);
			m++;
		}
		result[0] = m;
		result[1] = (int) last;
		return result;
	}

	private int[] getJalaliYearMonthDayWith(long days) {
		return new int[] { getDaysOfYear(days)[0], getDayOfMonth(days)[0], getDayOfMonth(days)[1], };
	}

	private int getJalaliMonthLength(int year, int month) {
		return month <= 6 ? 31 : month <= 11 ? 30 : isJalaliLeapYear(year) ? 30 : 29;
	}

	private long getJalaliCountDays(int year, int month, int day) {
		year = year == 0 ? 1 : year;
		int dy = (year - 1) * 365 + getJalaliCountLeap(year);
		for (int i = 1; i < month; i++) {
			dy += getJalaliMonthLength(year, i);
		}
		return (dy + day);
	}

	private int[] getDaysOfYear(long days) {
		int[] result = new int[2];
		long last = days;
		int y = 1;
		while (last > 366) {
			last -= 365;
			last = isJalaliLeapYear(y) ? last - 1 : last;
			y++;
		}
		if ((last == 366) && (!(isJalaliLeapYear(y)))) {
			last -= 365;
			y++;
		}
		result[0] = y;
		result[1] = (int) last;
		return result;
	}

	private int getFirstDayOfYear(long days) {
		int no = (int) (days - getDaysOfYear(days)[1] + 6) % 7;
		return no == 0 ? 7 : no;
	}

	/*
	 * First day of month is : Sunday Monday or .....
	 */
	private int getFirstDayOfMonth(long days) {
		long firstdayofmonth = days - getDayOfMonth(days)[1];
		int no = (int) (firstdayofmonth - 1) % 7;
		return no == 0 ? 7 : no;
	}

	private int getDayOfWeek() {
		int a = super.get(Calendar.DAY_OF_WEEK) + 1;
		return a > 7 ? 1 : a;
	}

	private int getWeekOfMonth(long days) {
		int day = (getDayOfMonth(days)[1] + getFirstDayOfMonth(days) - 1);
		int no = day % 7 == 0 ? day / 7 - 1 : day / 7;
		return no + 1;
	}

	private int getDayOfWeekInMonth(long days) {
		int day = (getDayOfMonth(days)[1]);
		int no = day % 7 == 0 ? day / 7 : day / 7 + 1;
		return no;
	}

	private int getWeekOfYear(long days) {
		int day = (getDaysOfYear(days)[1] + getFirstDayOfYear(days) - 1);
		int no = day % 7 == 0 ? day / 7 - 1 : day / 7;
		return no + 1;
	}

	// ======================= End Jalali Part ============================

	// ======================= Start Gregorian Part =======================
	private static final int MONTH_LENGTH[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	private static final int LEAP_MONTH_LENGTH[] = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	private int getGregorianCountLeap(int year) {
		int c = 0;
		for (int i = 1; i < year; i++) {
			if (isLeapYear(i)) {
				c++;
			}
		}
		return c;
	}

	private int getGregorianMonthLength(int year, int month) {
		return isLeapYear(year) ? LEAP_MONTH_LENGTH[month] : MONTH_LENGTH[month];
	}

	private long getGregorianCountDays(int year, int month, int day) {
		month++;// jan = 0
		int dy = (year - 1) * 365 + getGregorianCountLeap(year);
		for (int i = 1; i < month; i++) {
			dy += getGregorianMonthLength(year, i);
		}
		return (dy + day);
	}

	private JalaliCalendar setGregorianYearMonthDayWith(long days) {
		long last = days;
		int y = 1, m = 1;
		// محاسبه سال
		while (last > 366) {
			last -= 365;
			last = super.isLeapYear(y) ? last - 1 : last;
			y++;
		}
		if ((last == 366) && (!(super.isLeapYear(y)))) {
			y++;
			last -= 365;
		}
		while (last > getGregorianMonthLength(y, m)) {
			last -= getGregorianMonthLength(y, m);
			m++;
		}
		super.set(Calendar.YEAR, y);
		super.set(Calendar.MONTH, m - 1);
		super.set(Calendar.DAY_OF_MONTH, (int) last);
		return this;
	}

	// ======================= End Gregorian Part =======================

	// ======================= Start Convert Part =======================
	private long getGregorianCurrentCountDay() {
		return getGregorianCountDays(super.get(Calendar.YEAR), super.get(Calendar.MONTH),
				super.get(Calendar.DAY_OF_MONTH));
	}

	private void setGregorianChange(int jyear, int jmonth, int jday) {
		setGregorianYearMonthDayWith(getJalaliCountDays(jyear, jmonth, jday) + BASE_NUMBER);
	}

	private long getJalaliCurrentCountDay() {
		return getGregorianCountDays(super.get(Calendar.YEAR), super.get(Calendar.MONTH),
				super.get(Calendar.DAY_OF_MONTH) - BASE_NUMBER);
	}

	private int[] getJalaliYearMonthDay() {
		return getJalaliYearMonthDayWith(getJalaliCurrentCountDay());
	}

	// ======================= End Convert Part ==========================

	// ======================= Start Override Part =======================

	@Override
	public int get(int field) {
		switch (field) {
		case 100:// JALALI_YAER
			return getJalaliYearMonthDay()[0];
		case 101:// JALALI_MONTH
			return getJalaliYearMonthDay()[1];
		case 102:// JALALI_DAY
			return getJalaliYearMonthDay()[2];
		case 103:// JALALI_WEEK_OF_MONTH
			return getWeekOfMonth(getJalaliCurrentCountDay());
		case 104:// JALALI_WEEK_OF_YEAR
			return getWeekOfYear(getJalaliCurrentCountDay());
		case 105:// JALALI_DAY_OF_WEEK
			return getDayOfWeek();
		case 106:// JALALI_DAY_OF_WEEK_IN_MONTH
			return getDayOfWeekInMonth(getJalaliCurrentCountDay());
		case 107:// JALALI_DAY_OF_MONTH
			return getDayOfMonth(getJalaliCurrentCountDay())[1];
		case 108:// JALALI_DAY_OF_YEAR
			return getDaysOfYear(getJalaliCurrentCountDay())[1];
		case 109:// JALALI_FIRST_DAY_OF_YEAR
			return getFirstDayOfYear(getJalaliCurrentCountDay());
		case 110:// JALALI_FIRST_DAY_OF_MONTH
			return getFirstDayOfMonth(getJalaliCurrentCountDay());
		}
		return super.get(field);
	}

	@Override
	public void set(int field, int value) {
		switch (field) {
		case 100:// JALALI_YAER
			setGregorianChange(field, JALALI_MONTH, JALALI_DAY);
			return;
		case 101:// JALALI_MONTH
			setGregorianChange(JALALI_YAER, field, JALALI_DAY);
			return;
		case 102:// JALALI_DAY
			setGregorianChange(JALALI_YAER, JALALI_MONTH, field);
			return;
		case 107:// JALALI_DAY_OF_MONTH
			setGregorianChange(JALALI_YAER, field, JALALI_DAY);
			return;
		}
		super.set(field, value);
	}

	// @Override
	// public String toString() {
	// return Numbers.getTextPersian(toString("yyyy/MM/dd"));
	// }
	//
	public String toString(String format) {
		String[] strDayOfMonth = new String[] { "", "اول", "دوم", "سوم", "چهارم", "پنجم", "ششم", "هفتم", "هشتم", "نهم",
				"دهم", "یازدهم", "دوازدهم", "سیزدهم", "چهاردهم", "پانزدهم", "شانزدهم", "هفدهم", "هجدهم", "نوزدهم",
				"بیستم", "بیست و یکم", "بیست و دوم", "بیست و سوم", "بیست و چهارم", "بیست و پنجم", "بیست و ششم",
				"بیست و هفتم", "بیست و هشتم", "بیست و نهم", "سی ام", "سی و یکم" };
		String[] strDayName = new String[] { "", "شنبه", "یک شنبه", "دو شنبه", "سه شنبه", "چهار شنبه", "پنج شنبه",
				"جمعه", };
		String[] strMonthName = new String[] { "", "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر",
				"آبان", "آذر", "دی", "بهمن", "اسفند" };

		int[] ydm = getJalaliYearMonthDay();
		String by = String.valueOf(ydm[0]);
		String bm = String.valueOf(ydm[1]);
		String bd = String.valueOf(ydm[2]);
		String be = strDayName[getDayOfWeek()];
		String ba = this.get(Calendar.AM_PM) == 0 ? "قبل از ظهر" : "بعد از ظهر";
		List<String> ys = new ArrayList<String>();
		List<String> ms = new ArrayList<String>();
		List<String> ds = new ArrayList<String>();
		// List<String> as = new ArrayList<String>();

		List<Integer> ints = count('y', format);
		for (Integer i : ints) {
			if (i > 4) {
				ys.add(new Numeric2TextFa(by).getText());
			} else {
				ys.add(by);
			}
		}
		ints = count('M', format);
		for (Integer i : ints) {
			if (i > 2) {
				ms.add(strMonthName[ydm[1]]);
			} else if (i == 2) {
				ms.add(bm.length() == 2 ? bm : "0" + bm);
			} else {
				ms.add(bm);
			}
		}
		ints = count('d', format);
		for (Integer i : ints) {
			if (i > 2) {
				ds.add(strDayOfMonth[ydm[2]]);
			} else if (i == 2) {
				ds.add(bd.length() == 2 ? bd : "0" + bd);
			} else {
				ds.add(bd);
			}
		}
		format = replaceFull('y', format);
		format = replaceFull('M', format);
		format = replaceFull('d', format);
		format = replaceFull('a', format);
		format = replaceFull('E', format);
		DateFormat dateFormat = new SimpleDateFormat(format);
		String result = dateFormat.format(super.getTime());
		result = result.replace("E", be);
		result = result.replace("a", ba);

		if (ys.size() != 0) {
			while (result.indexOf('y') != -1) {
				result = result.replaceFirst("y", ys.remove(0));
			}
		}

		if (ms.size() != 0) {
			while (result.indexOf('M') != -1) {
				result = result.replaceFirst("M", ms.remove(0));
			}
		}

		if (ds.size() != 0) {
			while (result.indexOf('d') != -1) {
				result = result.replaceFirst("d", ds.remove(0));
			}
		}

		return result;
	}

	private List<Integer> count(char ch, String str2) {
		List<Integer> result = new ArrayList<Integer>();
		int l = str2.length();
		int c = 0;
		for (int i = 0; i < l; i++)
			if (str2.charAt(i) == ch) {
				c++;
			} else if (c != 0) {
				result.add(c);
				c = 0;
			}
		if (c != 0) {
			result.add(c);
			c = 0;
		}
		return result;
	}

	private String replaceFull(char ch, String str2) {
		String result = str2;
		int l = str2.length();
		String c = "";
		for (int i = 0; i < l; i++)
			if (str2.charAt(i) == ch) {
				c += ch;
			} else if (!c.equals("")) {
				result = result.replaceFirst(c, "'" + ch + "'");
				c = "";
			}
		if (!c.equals("")) {
			result = result.replaceFirst(c, "'" + ch + "'");
			c = "";
		}
		return result;
	}

	// ======================= End Override Part =======================
	public long jalaliCompareTo(Calendar anotherCalendar) {
		return this.getTimeInMillis() - anotherCalendar.getTimeInMillis();
	}

	/*
	 * -1 = less than<br/> 0 = between<br/> 1 = equal Argument 1<br/> 2 = equal
	 * Argument 2<br/> 3 = Greater than
	 */
	public int jalaliCompareTo(Calendar anotherCalendar1, Calendar anotherCalendar2) {
		long my = this.getTimeInMillis();
		long a = anotherCalendar1.getTimeInMillis();
		long b = anotherCalendar2.getTimeInMillis();
		if (my == a) {
			return -1;
		}
		if (my == b) {
			return 1;
		}
		if (my < a) {
			return -2;
		}
		if (my > b) {
			return 2;
		}
		return 0;
	}

	public JalaliCalendar getJalaliCalendar(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		super.setTimeInMillis(calendar.getTimeInMillis());
		return this;
	}

	public JalaliCalendar getJalaliCalendar(long millisecond) {
		super.setTimeInMillis(millisecond);
		return this;
	}

	// public String getPretty() {
	// PrettyTime prettyTime = new PrettyTime(Locales.getLocale());
	// return Numbers.getTextPersian(prettyTime.format(getTime()));
	// }

	// public String getDatePretty() {
	// return getSunecityDate() + " (" + getPretty() + ")";
	// }
	//
	// public String getDateTimePretty() {
	// return getSunecityTime() + " " + getSunecityDate() + " (" + getPretty()
	// + ")";
	// }

	// public String getSunecityDate() {
	// return toString();
	// }
	//
	// public String getSunecityDateTime() {
	// return getSunecityDate() + " " + getSunecityTime();
	// }

	// public String getSunecityTime() {
	// return Numbers.getTextPersian(Times.getTime(this));
	// }
	public class Numeric2TextFa {

		private final String[] YEKAN_TA_19 = { "", "يک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه", "ده",
				"يازده", "دوازده", "سيزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده" };

		private final String[] DAHGAN = { "", "", "بيست", "سي", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود" };

		private final String[] SADGAN = { "", "یک صد", "دويست", "سيصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد",
				"نهصد" };
		/*
		 * Million Milliard Billion Billiard Trillion Trilliard Quadrillion Quadrilliard
		 * Quintillion Quintilliard Sextillion Sextilliard Septillion Septilliard
		 * Octillion Octilliard Nonillion Nonilliard Decillion Decilliard Undecillion
		 * Undecilliard Duodecillion Duodecilliard Tredecillion Tredecilliard
		 * Quattuordecillion Quattuordecilliard Quindecillion Quindecilliard
		 * Sexdecillion Sexdecilliard Septendecillion Septendecilliard Octodecillion
		 * Octodecilliard Novemdecillion Novemdecilliard Vigintillion Vigintilliard
		 * Quinquavigintilliard Trigintilliard Quinquatrigintilliard Quadragintilliard
		 * Quinquaquadragintilliard Quinquagintilliard Unquinquagintillion
		 * Unquinquagintilliard Duoquinquagintillion Quinquaquinquagintilliard
		 * Sexaquinquagintillion Sexagintilliard Unsexagintillion Quinquasexagintilliard
		 * Septuagintilliard Quinquaseptuagintilliard Octogintilliard
		 * Quinquaoctogintilliard Nonagintilliard Quinquanonagintilliard Centilliard
		 * Quinquagintacentilliard Ducentilliard Quinquagintaducentilliard
		 * Trecentilliard Quinquagintatrecentilliard Quadringentilliard
		 * Quinquagintaquadringentilliard Quingentilliard
		 */
		private final String[] DEGREE = { "", "هزار", "میلیون", "میلیارد", "بیلیون", "بیلیارد", "ترلیون", "ترلیارد",
				"کوادرلیون", "کوادرلیارد", "سکسیلیون", "سکسیلیارد", "سپتلیون", "سپتیلیارد", "اوکتیلیون", "اوکتلیارد",
				"نانلیون", "نانلیارد", "دکلیون", "دکلیارد", "آندکلیون", "آندکلیارد", "دودکلیون", "دودکلیارد",
				"تردسلیون", "تردسلیارد", "کواتتوردسلیون", "کواتتوردسلیارد", "کویندسلیون", "کویندسلیارد", "سکسدسلیون",
				"سکسدسلیارد", "سپتندسیلیون", "سپتندسیلیارد", "اوکتودسلیون", "اوکتودسلیارد", "نومدسلیون", "نومدسلیارد",
				"ویجینتلیون", "ویجینتلیارد", "کوینکواویجینتلیون", "کوینکواویجینتلیارد", "تریجینتیلیون", "تریجینتیلیارد",
				"کوینکواتریجینتیلیون", "کوینکواتریجینتیلیارد", "کوادراجینتیلیون", "کوادراجینتیلیارد",
				"کوینکواکوادراجینتیلیون", "کوینکواکوادراجینتیلیارد", "کوینکواجینتلیون", "کوینکواجینتلیارد",
				"انکوینکواجینتلیون", "انکوینکواجینتلیارد", "دوکوینکواجینتیلیون", "دوکوینکواجینتیلیارد", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };

		private String result = "";

		public Numeric2TextFa() {
		}

		public Numeric2TextFa(String no) {
			this.result = noToTxt(no);
		}

		public Numeric2TextFa(Double no) {
			this.result = noToTxt(no.toString());
		}

		public String toString() {
			return this.result;
		}

		public void setNumber(int no) {
			this.result = noToTxt(String.valueOf(no));
		}

		public void setNumber(String no) {
			this.result = noToTxt(no);
		}

		public String getText() {
			return this.result;
		}

		private String number1Digit(int no1Dig) {
			String result = YEKAN_TA_19[no1Dig];
			return result.trim();
		}

		private String number2Digit(int no2Dig) {
			String str = String.valueOf(no2Dig).trim();
			String result = "";
			if (no2Dig < 20) {
				result = YEKAN_TA_19[no2Dig];
			} else {
				byte dah = Byte.valueOf(String.valueOf(str.charAt(0)));
				if (str.charAt(1) == '0') {
					result = DAHGAN[dah];
				} else {
					result = DAHGAN[dah] + " و " + number1Digit(Integer.parseInt(String.valueOf(str.charAt(1))));
				}
			}
			return result.trim();
		}

		private String number3Digit(int no3Dig) {
			String str = String.valueOf(no3Dig).trim();
			String result = "";
			byte sad = Byte.valueOf(String.valueOf(str.charAt(0)));
			if ((str.charAt(1) == '0') && (str.charAt(2) == '0')) {
				return SADGAN[sad].trim();
			}
			result = SADGAN[sad].trim() + " و "
					+ number2Digit(Byte.valueOf(String.valueOf(str.charAt(1)) + String.valueOf(str.charAt(2))));
			return result.trim();
		}

		private String prossNumber(int no) {
			String str = String.valueOf(no).trim();
			int len = str.length();
			String result = "";
			switch (len) {
			case 1:
				result = number1Digit(no);
				break;
			case 2:
				result = number2Digit(no);
				break;
			case 3:
				result = number3Digit(no);
				break;
			}
			return result.trim();
		}

		public String noToTxt(String no) {
			String num = no.trim();
			int c = 0;
			long[] nums = new long[24];
			String res = "";
			boolean isNegative = false;

			if ((num.length() == 0) || (num.length() > 72))
				return "";
			if (num.charAt(0) == '-') {
				isNegative = true;
				num = num.substring(1, num.length()).trim();
			}

			if ((num.length() == 1) && (num == "0")) {
				return "صفر";
			}

			for (int i = num.length() - 1; i >= 0; i -= 3) {
				String tr = "";
				if (i - 2 >= 0) {
					tr += num.charAt(i - 2);
				}
				if (i - 1 >= 0) {
					tr += num.charAt(i - +1);
				}
				if (i >= 0) {
					tr += num.charAt(i);
				}

				try {
					if (tr != "") {
						nums[c] = Long.parseLong(tr);
					}
				} catch (Exception ex) {
				}
				c++;
			}

			for (int i = 0; i <= c - 1; i++) {
				if (nums[i] == 0) {
					res = prossNumber((int) nums[i]) + res;
				} else {
					res = prossNumber((int) nums[i]) + " " + DEGREE[i] + " و " + res;
				}
			}
			try {
				if (isNegative) {
					res = (" منفی " + res.substring(0, res.length() - 3)).trim();
				} else {
					res = res.substring(0, res.length() - 3);
				}
			} catch (Exception ex) {
			}
			return res;
		}

	}

}
